###############################################################################
# Required Variables
###############################################################################

variable "account" {
  type        = string
  description = "The full name of the Storage Account that will contain this storage blob."
}

variable "container" {
  type        = string
  description = "The full name of the Storage Account container that will contain this storage blob."
}

###############################################################################
# Optional Variables
###############################################################################

variable "key" {
  type        = string
  default     = "default"
  description = "The full name and path of this storage blob key."
}

variable "tier" {
  type        = string
  default     = "Hot"
  description = "The access tier of the storage blob. Possible values are 'Archive', 'Cool' and 'Hot'."
}

variable "type" {
  type        = string
  default     = "Block"
  description = "The type of the storage blob to be created. Possible values are 'Append', 'Block' or 'Page'."
}

variable "content_type" {
  type        = string
  default     = "application/octet-stream"
  description = "The content type of the storage blob. Ignored if 'source_url' is specified."
}

###############################################################################

variable "source_url" {
  type        = string
  default     = null
  description = "The URL to create the storage blob from."
}

variable "source_file" {
  type        = string
  default     = null
  description = "The file to create the storage blob from. Ignored if 'source_url' is specified."
}

variable "source_content" {
  type        = string
  default     = ""
  description = "The content of the blob to create. Ignored if 'source_url' or 'source_file' are specified."
}

###############################################################################
# Locals
###############################################################################

locals {
  source_file    = var.source_url == null ? var.source_file : null
  content_type   = var.source_url == null ? var.content_type : null
  source_content = (var.source_url == null) && (local.source_file == null) ? var.source_content : null
}

###############################################################################
# Resources
###############################################################################

resource "azurerm_storage_blob" "object" {
  name                   = var.key
  type                   = var.type
  source                 = local.source_file
  source_uri             = var.source_url
  access_tier            = var.tier
  content_type           = local.content_type
  source_content         = local.source_content
  storage_account_name   = var.account
  storage_container_name = var.container
}

###############################################################################
# Outputs
###############################################################################

output "source_url" {
  value = var.source_url
}

output "source_file" {
  value = local.source_file
}

output "source_content" {
  value = local.source_content
}

###############################################################################

output "id" {
  value = azurerm_storage_blob.object.id
}

output "key" {
  value = azurerm_storage_blob.object.name
}

output "url" {
  value = azurerm_storage_blob.object.url
}

output "tier" {
  value = azurerm_storage_blob.object.access_tier
}

output "type" {
  value = azurerm_storage_blob.object.type
}

output "account" {
  value = azurerm_storage_blob.object.storage_account_name
}

output "container" {
  value = azurerm_storage_blob.object.storage_container_name
}

output "content_type" {
  value = azurerm_storage_blob.object.content_type
}

###############################################################################
