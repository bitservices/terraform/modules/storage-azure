<!---------------------------------------------------------------------------->

# blob

#### Create and manage blobs within [Blob Storage].

-------------------------------------------------------------------------------

Source **`gitlab.com/bitservices/storage/azure//blob`**

-------------------------------------------------------------------------------

### Example Usage

```
variable "owner"    { default = "terraform@bitservices.io" }
variable "company"  { default = "BITServices Ltd"          }
variable "location" { default = "uksouth"                  }

module "my_resource_group" {
  source   = "gitlab.com/bitservices/group/azure//resource"
  name     = "foobar"
  owner    = var.owner
  company  = var.company
  location = var.location
}

module "my_storage_account" {
  source   = "gitlab.com/bitservices/storage/azure//account"
  name     = "foobarsa1"
  group    = module.my_resource_group.name
  owner    = var.owner
  company  = var.company
  location = var.location
}

module "my_storage_container" {
  source  = "gitlab.com/bitservices/storage/azure//container"
  account = module.my_storage_account.name
}

module "my_storage_blob" {
  source    = "gitlab.com/bitservices/storage/azure//blob"
  key       = "foo/bar.blob"
  account   = module.my_storage_account.name
  container = module.my_storage_container.name
}
```

<!---------------------------------------------------------------------------->

[Blob Storage]: https://azure.microsoft.com/services/storage/blobs/

<!---------------------------------------------------------------------------->
