<!---------------------------------------------------------------------------->

# storage (azure)

<!---------------------------------------------------------------------------->

## Description

Manages [Storage] resources on [Azure], such as [Blob Storage] and
[File Shares].

<!---------------------------------------------------------------------------->

## Modules

* [account](account/README.md) - Manage [Storage] accounts.
* [blob](blob/README.md) - Create and manage blobs within [Blob Storage].
* [container](container/README.md) - Manage [Storage] account [Blob Storage].
* [share](share/README.md) - Manage [Storage] account [File Shares].

<!---------------------------------------------------------------------------->

[Azure]:        https://azure.microsoft.com/
[Storage]:      https://azure.microsoft.com/services/storage/
[File Shares]:  https://azure.microsoft.com/services/storage/files/
[Blob Storage]: https://azure.microsoft.com/services/storage/blobs/

<!---------------------------------------------------------------------------->
