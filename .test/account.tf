###############################################################################
# Modules
###############################################################################

module "account" {
  source   = "../account"
  name     = "foobar"
  group    = local.group
  owner    = local.owner
  company  = local.company
  location = local.location
}

###############################################################################
