###############################################################################
# Required Variables
###############################################################################

variable "account" {
  type        = string
  description = "The full name of the Storage Account that will contain this file share."
}

###############################################################################
# Optional Variables
###############################################################################

variable "name" {
  type        = string
  default     = "default"
  description = "The full name of this storage container."
}

variable "quota" {
  type        = number
  default     = 10
  description = "The limit in gigabytes of how big this file share can grow."
}

###############################################################################
# Resources
###############################################################################

resource "azurerm_storage_share" "object" {
  name                 = var.name
  quota                = var.quota
  storage_account_name = var.account
}

###############################################################################
# Outputs
###############################################################################

output "id" {
  value = azurerm_storage_share.object.resource_manager_id
}

output "url" {
  value = azurerm_storage_share.object.id
}

output "name" {
  value = azurerm_storage_share.object.name
}

output "quota" {
  value = azurerm_storage_share.object.quota
}

output "account" {
  value = azurerm_storage_share.object.storage_account_name
}

###############################################################################
