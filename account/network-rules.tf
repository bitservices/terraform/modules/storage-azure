###############################################################################
# Optional Variables
###############################################################################

variable "network_rules_bypass" {
  type        = list(string)
  default     = ["None"]
  description = "List of traffic types that bypass the network rules. Can either be a combination of 'Logging', 'Metrics', 'AzureServices' or just 'None'. Ignored if 'public' is 'true'."
}

variable "network_rules_public" {
  type        = bool
  default     = true
  description = "Should the Storage Account be open to all network traffic? All other network rules variables are ignored if this is set to 'true'."
}

variable "network_rules_ipv4_cidrs" {
  type        = list(string)
  default     = []
  description = "List of IPv4 CIDR ranges that are permitted access to this Storage Account. Ignored if 'public' is 'true'."
}

###############################################################################
# Locals
###############################################################################

locals {
  network_rules_action     = var.network_rules_public ? "Allow" : "Deny"
  network_rules_bypass     = var.network_rules_public ? ["None"] : var.network_rules_bypass
  network_rules_ipv4_cidrs = var.network_rules_public ? [] : var.network_rules_ipv4_cidrs
}

###############################################################################
# Resources
###############################################################################

resource "azurerm_storage_account_network_rules" "object" {
  bypass             = local.network_rules_bypass
  ip_rules           = local.network_rules_ipv4_cidrs
  default_action     = local.network_rules_action
  storage_account_id = azurerm_storage_account.object.id
}

###############################################################################
# Outputs
###############################################################################

output "network_rules_bypass" {
  value = local.network_rules_bypass
}

output "network_rules_public" {
  value = var.network_rules_public
}

output "network_rules_ipv4_cidrs" {
  value = local.network_rules_ipv4_cidrs
}

###############################################################################

output "network_rules_action" {
  value = local.network_rules_action
}

###############################################################################

output "network_rules_id" {
  value = azurerm_storage_account_network_rules.object.id
}

###############################################################################
