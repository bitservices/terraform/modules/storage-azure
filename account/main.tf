###############################################################################
# Required Variables
###############################################################################

variable "name" {
  type        = string
  description = "The full name of the Storage Account. Must be globally unique."
}

variable "group" {
  type        = string
  description = "The full name of the resource group that will contain this Storage Account."
}

variable "owner" {
  type        = string
  description = "Owner of the resource."
}

variable "company" {
  type        = string
  description = "Company the resource belogs to."
}

variable "location" {
  type        = string
  description = "Datacentre location for this Storage Account."
}

###############################################################################
# Optional Variables
###############################################################################

variable "kind" {
  type        = string
  default     = "StorageV2"
  description = "The kind of Storage Account this is going to be."
}

variable "tier" {
  type        = string
  default     = "Standard"
  description = "Defines the Tier to use for this Storage Account."
}

variable "public" {
  type        = bool
  default     = false
  description = "Permit public containers, blobs, etc to be created within this Storage Account."
}

variable "force_https" {
  type        = bool
  default     = true
  description = "Boolean flag which forces HTTPS if enabled."
}

variable "replication" {
  type        = string
  default     = "LRS"
  description = "An identifer for this group of resources. Is used as a suffix to the resource group name."
}

variable "soft_delete_days" {
  type        = number
  default     = null
  description = "Number of days to retain deleted or overwritten objects. null turns soft delete off."
}

###############################################################################
# Locals
###############################################################################

locals {
  tier = var.kind == "FileStorage" ? "Premium" : var.tier
}

###############################################################################
# Resources
###############################################################################

resource "azurerm_storage_account" "object" {
  name                            = var.name
  location                        = var.location
  account_kind                    = var.kind
  account_tier                    = local.tier
  resource_group_name             = var.group
  account_replication_type        = var.replication
  enable_https_traffic_only       = var.force_https
  allow_nested_items_to_be_public = var.public

  tags = {
    "Kind"         = var.kind
    "Name"         = var.name
    "Tier"         = local.tier
    "Group"        = var.group
    "Owner"        = var.owner
    "Public"       = var.public ? "Enabled" : "Disabled"
    "Company"      = var.company
    "Location"     = var.location
    "Replication"  = var.replication
    "Subscription" = data.azurerm_subscription.current.display_name
  }

  blob_properties {
    dynamic "delete_retention_policy" {
      for_each = var.soft_delete_days == null ? [] : [null]

      content {
        days = var.soft_delete_days
      }
    }
  }
}

###############################################################################
# Outputs
###############################################################################

output "owner" {
  value = var.owner
}

output "company" {
  value = var.company
}

###############################################################################

output "kind" {
  value = var.kind
}

output "tier" {
  value = local.tier
}

output "public" {
  value = var.public
}

output "force_https" {
  value = var.force_https
}

output "replication" {
  value = var.replication
}

output "soft_delete_days" {
  value = var.soft_delete_days
}

###############################################################################

output "id" {
  value = azurerm_storage_account.object.id
}

output "name" {
  value = azurerm_storage_account.object.name
}

output "group" {
  value = azurerm_storage_account.object.resource_group_name
}

output "identity" {
  value = azurerm_storage_account.object.identity
}

###############################################################################

output "location_primary" {
  value = azurerm_storage_account.object.primary_location
}

output "location_secondary" {
  value = azurerm_storage_account.object.secondary_location
}

###############################################################################

output "access_key_primary" {
  sensitive = true
  value     = azurerm_storage_account.object.primary_access_key
}

output "access_key_secondary" {
  sensitive = true
  value     = azurerm_storage_account.object.secondary_access_key
}

###############################################################################

output "connection_string_primary" {
  sensitive = true
  value     = azurerm_storage_account.object.primary_connection_string
}

output "connection_string_secondary" {
  sensitive = true
  value     = azurerm_storage_account.object.secondary_connection_string
}

###############################################################################

output "blob_host_primary" {
  value = azurerm_storage_account.object.primary_blob_host
}

output "blob_host_secondary" {
  value = azurerm_storage_account.object.secondary_blob_host
}

output "blob_endpoint_primary" {
  value = azurerm_storage_account.object.primary_blob_endpoint
}

output "blob_endpoint_secondary" {
  value = azurerm_storage_account.object.secondary_blob_endpoint
}

output "blob_connection_string_primary" {
  value = azurerm_storage_account.object.primary_blob_connection_string
}

output "blob_connection_string_secondary" {
  value = azurerm_storage_account.object.secondary_blob_connection_string
}

###############################################################################

output "queue_host_primary" {
  value = azurerm_storage_account.object.primary_queue_host
}

output "queue_host_secondary" {
  value = azurerm_storage_account.object.secondary_queue_host
}

output "queue_endpoint_primary" {
  value = azurerm_storage_account.object.primary_queue_endpoint
}

output "queue_endpoint_secondary" {
  value = azurerm_storage_account.object.secondary_queue_endpoint
}

###############################################################################

output "table_host_primary" {
  value = azurerm_storage_account.object.primary_table_host
}

output "table_host_secondary" {
  value = azurerm_storage_account.object.secondary_table_host
}

output "table_endpoint_primary" {
  value = azurerm_storage_account.object.primary_table_endpoint
}

output "table_endpoint_secondary" {
  value = azurerm_storage_account.object.secondary_table_endpoint
}

###############################################################################

output "file_host_primary" {
  value = azurerm_storage_account.object.primary_file_host
}

output "file_host_secondary" {
  value = azurerm_storage_account.object.secondary_file_host
}

output "file_endpoint_primary" {
  value = azurerm_storage_account.object.primary_file_endpoint
}

output "file_endpoint_secondary" {
  value = azurerm_storage_account.object.secondary_file_endpoint
}

###############################################################################

output "dfs_host_primary" {
  value = azurerm_storage_account.object.primary_dfs_host
}

output "dfs_host_secondary" {
  value = azurerm_storage_account.object.secondary_dfs_host
}

output "dfs_endpoint_primary" {
  value = azurerm_storage_account.object.primary_dfs_endpoint
}

output "dfs_endpoint_secondary" {
  value = azurerm_storage_account.object.secondary_dfs_endpoint
}

###############################################################################

output "web_host_primary" {
  value = azurerm_storage_account.object.primary_web_host
}

output "web_host_secondary" {
  value = azurerm_storage_account.object.secondary_web_host
}

output "web_endpoint_primary" {
  value = azurerm_storage_account.object.primary_web_endpoint
}

output "web_endpoint_secondary" {
  value = azurerm_storage_account.object.secondary_web_endpoint
}

###############################################################################
