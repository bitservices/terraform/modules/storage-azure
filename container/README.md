<!---------------------------------------------------------------------------->

# container

#### Manage [Storage] account [Blob Storage]

-------------------------------------------------------------------------------

Source **`gitlab.com/bitservices/storage/azure//container`**

-------------------------------------------------------------------------------

### Example Usage

```
variable "owner"    { default = "terraform@bitservices.io" }
variable "company"  { default = "BITServices Ltd"          }
variable "location" { default = "uksouth"                  }

module "my_resource_group" {
  source   = "gitlab.com/bitservices/group/azure//resource"
  name     = "foobar"
  owner    = var.owner
  company  = var.company
  location = var.location
}

module "my_storage_account" {
  source   = "gitlab.com/bitservices/storage/azure//account"
  name     = "foobarsa1"
  group    = module.my_resource_group.name
  owner    = var.owner
  company  = var.company
  location = var.location
}

module "my_storage_container" {
  source  = "gitlab.com/bitservices/storage/azure//container"
  account = module.my_storage_account.name
}
```

<!---------------------------------------------------------------------------->

[Storage]:      https://azure.microsoft.com/services/storage/
[Blob Storage]: https://azure.microsoft.com/services/storage/blobs/

<!---------------------------------------------------------------------------->
