###############################################################################
# Required Variables
###############################################################################

variable "account" {
  type        = string
  description = "The full name of the Storage Account that will contain this storage container."
}

###############################################################################
# Optional Variables
###############################################################################

variable "name" {
  type        = string
  default     = "default"
  description = "The full name of this storage container."
}

variable "public_access" {
  type        = string
  default     = "private"
  description = "What level of public access is configured for this storage container. Use private for none."
}

###############################################################################
# Resources
###############################################################################

resource "azurerm_storage_container" "object" {
  name                  = var.name
  storage_account_name  = var.account
  container_access_type = var.public_access
}

###############################################################################
# Outputs
###############################################################################

output "public_access" {
  value = var.public_access
}

###############################################################################

output "id" {
  value = azurerm_storage_container.object.resource_manager_id
}

output "url" {
  value = azurerm_storage_container.object.id
}

output "name" {
  value = azurerm_storage_container.object.name
}

output "account" {
  value = azurerm_storage_container.object.storage_account_name
}

output "has_legal_hold" {
  value = azurerm_storage_container.object.has_legal_hold
}

output "has_immutability_policy" {
  value = azurerm_storage_container.object.has_immutability_policy
}

###############################################################################
